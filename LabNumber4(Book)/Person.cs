﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace LabNumber4_Book_
{
    abstract class Person
    {
        protected int _perscode;
        protected string _surname;
        public string surname { get { return _surname; } set { _surname = value; } }
        protected string _name;
        public string name { get { return _name; } set { _name = value; } }
        protected string _phonenumber;
        protected string _email;
        public int perscode { get { return _perscode; } set { _perscode = value; } }

        public abstract void AddNewPerson();
        public abstract void PrintAll();
        public abstract bool SearchPersonOperation(int searchedcode);
        /// <summary>
        /// Метод для видалення особи із файлу, приймає шляхи до файлів.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="pathtemp"></param>
        protected void DeletePerson(string path, string pathtemp)
        {
            while(true)
            {
                try
                {
                    Console.Write("Enter the code: ");
                    int id = Convert.ToInt32(Console.ReadLine());
                    if (SearchPersonOperation(id))
                    {
                        string line_to_delete = "";
                        Console.Write("Press 1 to confirm the deletion: ");
                        while(true)
                        {
                            try
                            {
                                int confirmdelete = Convert.ToInt32(Console.ReadLine());
                                if (confirmdelete == 1)
                                {
                                    bool delete = false;

                                    try
                                    {
                                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                                        {
                                            string line;
                                            List<string> words = new List<string>();
                                            while ((line = sr.ReadLine()) != null)
                                            {
                                                words = (line.Split('|')).ToList();
                                                perscode = Convert.ToInt32(words[0]);
                                                if (id == perscode)
                                                {
                                                    line_to_delete = line;
                                                    delete = true;
                                                    break;
                                                }
                                                else
                                                {
                                                    delete = false;
                                                }
                                            }
                                            sr.Close();
                                        }
                                        if (!delete)
                                        {
                                            Console.WriteLine("You entered incorrect code");
                                        }
                                    }
                                    catch
                                    {
                                        Console.WriteLine("Error opening file! Person.cs DeletePerson(1)");
                                    }
                                    string str = null;
                                    try
                                    {
                                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                                        {
                                            using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                                            {
                                                while ((str = sr.ReadLine()) != null)
                                                {
                                                    if (String.Compare(str, line_to_delete) == 0)
                                                    {
                                                        Console.WriteLine("Successful deleting.");
                                                        continue;
                                                    }
                                                    sw.WriteLine(str);
                                                }
                                                sw.Close();
                                            }
                                            sr.Close();
                                        }
                                        File.Delete(path);
                                        File.Move(pathtemp, path);
                                    }
                                    catch
                                    {
                                        Console.WriteLine("Error opening file! Person.cs DeletePerson(2)");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("The delete operation was canceled.");
                                }
                                break;
                            }
                            catch(Exception)
                            {
                                Console.WriteLine("Input error.");
                            }
                        }                        
                    }
                    else
                    {
                        Console.WriteLine("Record does not exist.");
                    }
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
            
        }
        public abstract void EditPerson();
        /// <summary>
        /// Метод для генерації id особи.
        /// </summary>
        /// <returns>Випадкове число</returns>
        protected int GetPerscode()
        {
            Random random = new Random();

            return random.Next(1, 10000);
        }
        
    }
}
