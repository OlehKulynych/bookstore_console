﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace LabNumber4_Book_
{
    
    class Purchase
    {
        protected int _checkbookid;
        public int checkbookid { get { return _checkbookid; } set { _checkbookid = value; } }
        protected string _checkbookname;
        protected int _checkbookquantity;
        protected double _checkprice;
        protected double _totalprice;
        protected int _checkemployeeid;
        protected string _checkemployeesurname;
        protected string _checkemployeename;
        protected int _checkcustomerid;
        protected string _checkcustomersurname;
        protected string _checkcustomername;
        protected int _checkdiscount;
        protected bool isOrderEmpty = false;
        protected List<Purchase> check_books = new List<Purchase>();
        private Book book = new Book();
        private Employee employee = new Employee();
        private Customer customer = new Customer();

        protected List<int> _numberbooks = new List<int>(); 
        public Purchase()
        {
            checkbookid = -1;
            _checkbookname = null;
            _checkbookquantity = -1;
            _checkprice = -1;
        }
        public Purchase(int bookid, string bookname, int bookquantity, double bookprice)
        {
            checkbookid = bookid;
            _checkbookname = bookname;
            _checkbookquantity = bookquantity;
            _checkprice = bookprice;
        }
        /// <summary>
        /// Метод отримання даних товару із замовлення.
        /// </summary>
        /// <returns>Список даних</returns>
        public List<string> GetCheckData()
        {
            List<string> bookincheck = new List<string>();
            bookincheck.Add(checkbookid.ToString());
            bookincheck.Add(_checkbookname);
            bookincheck.Add(_checkbookquantity.ToString());
            bookincheck.Add(_checkprice.ToString());
            return bookincheck;
        }
        /// <summary>
        /// Метод покупки.
        /// </summary>
        protected void Order()
        {
            bool order = true;
            
            int i = 0;
            do
            {
                    try
                    {
                        Console.WriteLine("-----------------");
                        Console.WriteLine("Enter 1 if you want to add the book to the cart or another number to complete your purchase: ");
                        int operation = Convert.ToInt32(Console.ReadLine());
                        switch (operation)
                        {
                            case 1:
                                {
                                    while(true)
                                    {
                                        try
                                        {
                                            Console.WriteLine("-----------------");
                                            Console.Write("Enter the code: ");
                                            int code = Convert.ToInt32(Console.ReadLine());
                                            if (book.SearchedBookOperation(code))
                                            {
                                                while(true)
                                                {
                                                    try
                                                    {
                                                        Console.WriteLine("Add this book? Enter 1 to confirm. ");
                                                        int confirm = Convert.ToInt32(Console.ReadLine());
                                                        if (confirm == 1)
                                                        {
                                                            checkbookid = book.bookcode;
                                                            _checkbookname = book.title;
                                                            while(true)
                                                            {
                                                                try
                                                                {
                                                                    Console.WriteLine("Specify the number of books: ");
                                                                    _checkbookquantity = Convert.ToInt32(Console.ReadLine());
                                                                    if (_checkbookquantity > 0 && _checkbookquantity <= book.quantity)
                                                                    {
                                                                        _checkprice = _checkbookquantity * book.price;
                                                                        Purchase purchase = new Purchase(checkbookid, _checkbookname, _checkbookquantity, _checkprice);
                                                                        check_books.Add(purchase);
                                                                        _numberbooks.Add(_checkbookquantity);
                                                                        i++;
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                    break;
                                                                }
                                                                catch(Exception)
                                                                {
                                                                    Console.WriteLine("Input error.");
                                                                }
                                                            }
                                                            
                                                        }
                                                        else
                                                        {
                                                            isOrderEmpty = true;
                                                            
                                                        }
                                                        isOrderEmpty = false;
                                                        break;
                                                    }
                                                    catch(Exception)
                                                    {
                                                        Console.WriteLine("Input error.");
                                                    }
                                                }
                                                
                                            }
                                            else
                                            {
                                                isOrderEmpty = true;
                                            }
                                            break;
                                        }
                                        catch(Exception)
                                        {
                                            Console.WriteLine("Input error.");
                                        }
                                    }
                                    
                                }
                                break;
                            default:
                                {
                                    Console.WriteLine("-----------------");

                                    order = false;
                                }
                                break;
                        }
                    }
                    catch(Exception)
                    {
                        Console.WriteLine("Input error.");
                    }
                
            } while (order);
            Console.WriteLine("-----------------");
            if(check_books.Count != 0)
            {
                CustomerOrder();
                EmployeeOrder();
                PrintCheck(check_books);
                i = 0;
                bool delete = true;
                do
                {
                    try
                    {
                        Console.WriteLine("You have completed your purchase...To remove an item from the check, press 1...Any other number will complete the purchase...");
                        int complete = Convert.ToInt32(Console.ReadLine());
                        if (complete == 1)
                        {
                            if (check_books.Count == 0)
                            {
                                Console.WriteLine("The order is empty");
                                isOrderEmpty = true;
                            }
                            else
                            {
                                check_books = DeleteBookInCheck(check_books);
                                Console.WriteLine("-----------------");
                                if (check_books.Count != 0)
                                {
                                    PrintCheck(check_books);
                                }
                                else
                                {
                                    Console.WriteLine("The order is empty");
                                    isOrderEmpty = true;
                                    break;
                                }

                            }

                        }
                        else
                        {
                            foreach (Purchase p in check_books)
                            {
                                book.EditNumberCopies(_numberbooks[i], p.checkbookid);
                                i++;
                            }
                            delete = false;
                        }
                    }
                    catch(Exception)
                    {
                        Console.WriteLine("Input error.");
                    }
                    
                } while (delete);
                
            }
            else
            {
                Console.WriteLine("The order is empty");
                isOrderEmpty = true;
            }
                      
        }
        /// <summary>
        /// Метод визначення ціни, приймає список об'єктів товарів замовлення.
        /// </summary>
        /// <param name="purchases"></param>
        /// <returns>Повертає значення остаточної ціни з урахуванням знижки для певного покупця.</returns>
        private double Price(List<Purchase> purchases)
        {
            _totalprice = 0;
            var queue = new Queue<Purchase>();
            foreach(Purchase p in purchases)
            {
                
                queue.Enqueue(p);
            }

            foreach(Purchase p in queue)
            {
                    _totalprice += Convert.ToDouble(p.GetCheckData()[3]);
            }
            return _totalprice - ((_totalprice * _checkdiscount)/100);
        }
        /// <summary>
        /// Видаляє вибраний товар із замовлення.
        /// </summary>
        /// <param name="purchases"></param>
        /// <returns>Новий список об'єктів в замовленні.</returns>
        private List<Purchase> DeleteBookInCheck(List<Purchase> purchases)
        {
            while(true)
            {
                try
                {
                    Console.WriteLine("-----------------");
                    Console.Write("Enter the code: ");
                    int code = Convert.ToInt32(Console.ReadLine());
                    foreach (Purchase p in purchases.ToList())
                    {
                        var obj = (purchases.FirstOrDefault(p => p.checkbookid == code));
                        if (obj != null)
                        {
                            int index = purchases.IndexOf(obj);
                            purchases.RemoveAt(index);
                        }
                    }
                    
                    return purchases;
                    
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
            
        }
        /// <summary>
        /// Виводить дані замовлення, приймає список об'єктів замовлення.
        /// </summary>
        /// <param name="purchases"></param>
        protected void PrintCheck(List<Purchase> purchases)
        {

            var queue = new Queue<Purchase>();
            Console.WriteLine("\nYour order: ");
            Console.WriteLine("-----------------\n");
            foreach (Purchase p in purchases)
            {
                queue.Enqueue(p);
            }
            foreach(Purchase p in queue)
            {
                Console.WriteLine("Book code: " + p.checkbookid);
                Console.WriteLine("Title: " + p._checkbookname);
                Console.WriteLine("Quantity: " + p._checkbookquantity);
                Console.WriteLine("Price: " + p._checkprice);
                Console.WriteLine("-----------------\n");
            }
            Console.WriteLine("Customer: " + _checkcustomersurname + " " + _checkcustomername);
            Console.WriteLine("-----------------");
            Console.WriteLine("Employee: " + _checkemployeesurname + " " + _checkemployeename);
            Console.WriteLine("-----------------");
            Console.WriteLine("Total price: " + Price(purchases));
        }

       /// <summary>
       /// Метод визначення працівника, який обслуговував дане замовлення.
       /// </summary>
        private void EmployeeOrder()
        {
            bool employeeorder = true;
            do
            {
                try
                {
                    Console.Write("Enter employee ID: ");
                    int idemployee = Convert.ToInt32(Console.ReadLine());
                    if (employee.SearchPersonOperation(idemployee))
                    {
                        _checkemployeeid = employee.perscode;
                        _checkemployeesurname = employee.surname;
                        _checkemployeename = employee.name;
                        employeeorder = false;
                    }
                    else
                    {
                        Console.WriteLine("Such a employee is not registered.");

                    }
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
                

            } while (employeeorder);
        }
        /// <summary>
        /// Метод визначення клієнта, який робив дане замовлення.
        /// </summary>
        private void CustomerOrder()
        {
            bool customerorder = true;
            do
            {
                try
                {
                    Console.Write("Enter customer ID: ");
                    int idcustomer = Convert.ToInt32(Console.ReadLine());
                    if (customer.SearchPersonOperation(idcustomer))
                    {
                        _checkcustomerid = customer.perscode;
                        _checkcustomersurname = customer.surname;
                        _checkcustomername = customer.name;
                        _checkdiscount = customer.typediscount;
                        customerorder = false;

                    }
                    else
                    {
                        Console.WriteLine("Such a client is not registered.");
                        while(true)
                        {
                            try
                            {
                                Console.Write("Enter 1 if you want to register new: ");
                                int yesorno = Convert.ToInt32(Console.ReadLine());
                                if (yesorno == 1)
                                {
                                    customer.AddNewPerson();
                                    Console.WriteLine("Again...");
                                }
                                else
                                {
                                    Console.WriteLine("Again...");
                                }
                                break;
                            }
                            catch(Exception)
                            {
                                Console.WriteLine("Input error.");
                            }
                        }
                        
                    }
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
                
            } while (customerorder);
        }
    }
}
