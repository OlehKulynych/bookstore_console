﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

interface IBook
{
    void AddBook();
    void PrintDataBook();
    void DeleteBook();
    bool SearchedBookOperation(int searchedcode);
    void EditBook();
}

namespace LabNumber4_Book_
{
    class Book : IBook
    {
        private int _bookcode;
        public int bookcode { get { return _bookcode; } set { _bookcode = value; } }

        private string _title;
        public string title { get { return _title; } set { _title = value; } }
        private string _author;
        private string _genre;
        private int _publicationyear;
        private int _numberpages;
        private string _language;
        private int _quantity;
        private double _price;
        public double price { get { return _price; } set => _price = (value < 0) ? 0 : value; }
        private string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Book.txt");
        private string pathtemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp.txt");
        private string datastring;
        public int quantity
        {
            set => _quantity = (value < 0) ? 0 : value;
            get { return _quantity; }
        }

        protected int GetBookcode()
        {
            Random random = new Random();

            return random.Next(1, 1000000);
        }
        

        /// <summary>
        /// Записуємо дані про нашу книгу у вигляді строки в такому форматі.
        /// </summary>
        public void DataToString()
        {
            string datastring = $"{bookcode}|{title}|{_author}|{_genre}|{_publicationyear}|{_numberpages}|{_language}|{quantity}|{price}";
            this.datastring = datastring;
        }

        /// <summary>
        /// Запрошуємо ввід даних із клавіатури.
        /// </summary>
        public void EnterData()
        {
            while (true)
            {
                Console.Write("Enter a title for the book: ");
                title = Console.ReadLine();
                if (title == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                Console.Write("Enter the author: ");
                _author = Console.ReadLine();
                if (_author == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                Console.Write("Enter genre: ");
                _genre = Console.ReadLine();
                if (_genre == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                try
                {
                    Console.Write("Enter the year of publication: ");
                    _publicationyear = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
            while (true)
            {
                try
                {
                    Console.Write("Enter the number of pages: ");
                    _numberpages = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
            while (true)
            {
                Console.Write("Enter the language: ");
                _language = Console.ReadLine();
                if (_language == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            while(true)
            {
                try
                {
                    Console.Write("Enter the quantity: ");
                    quantity = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
            while(true)
            {
                try
                {
                    Console.Write("Enter the price: ");
                    price = Convert.ToDouble(Console.ReadLine());
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
            
        }

        /// <summary>
        /// Додаємо нову книгу і записуємо дані в файл.
        /// </summary>
        public void AddBook()
        {
            int book_id = GetBookcode();
            bookcode = book_id;
            EnterData();
            DataToString();
            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine(datastring);

                streamWriter.Close();
            }
        }
        /// <summary>
        /// Виводить інформацію про всі книги.
        /// </summary>
        public void PrintFile()
        {                       
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        PrintDataBook();
                    }
                    sr.Close();
                }                        
            }
            catch
            {
                Console.WriteLine("Error opening file!(Book.cs PrintFile())");
            }
        }
        /// <summary>
        /// Виводить інформацію про одну книгу
        /// </summary>
        public void PrintDataBook()
        {        
            Console.WriteLine("Book code: " + bookcode);
            Console.WriteLine("Title: " + title);
            Console.WriteLine("Author: " + _author);
            Console.WriteLine("Genre: " + _genre);
            Console.WriteLine("Year of publication: " + _publicationyear);
            Console.WriteLine("Number of pages: " + _numberpages);
            Console.WriteLine("Language: " + _language);
            Console.WriteLine("Quantity: " + quantity);
            Console.WriteLine("Price: " + price);
            Console.WriteLine("\n");
        }
        
        public void SearchBook()
        {
            while(true)
            {
                try
                {
                    Console.Write(" Enter code: ");
                    int searchedcode = Convert.ToInt32(Console.ReadLine());
                    SearchedBookOperation(searchedcode);
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
            
        }
        /// <summary>
        /// Шукає книгу серед файлу.
        /// </summary>
        /// <param name="searchedcode"></param>
        /// /// <returns>True or False</returns>
        public bool SearchedBookOperation(int searchedcode)
        {
            bool search = false;
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();

                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        if (searchedcode == _bookcode)
                        {
                            Console.WriteLine("Success! Record found. ");
                            search = true;
                            PrintDataBook();
                            break;
                        }
                        else
                        {
                            search = false;
                        }
                    }
                    sr.Close();
                  
                }
            }
            catch
            {
                Console.WriteLine("Error opening file! (Book.cs SearchedBookOperation()) ");
            }
            if (!search)
            {
                Console.WriteLine("Failed");
                return false;
            }
            return true;
        }
        /// <summary>
        /// Видаляє запис про книгу.
        /// </summary>
        public void DeleteBook()
        {
            while(true)
            {
                try
                {
                    Console.Write("Enter the code: ");
                    int id = Convert.ToInt32(Console.ReadLine());
                    if (SearchedBookOperation(id))
                    {
                        string line_to_delete = "";
                        while (true)
                        {
                            try
                            {
                                Console.Write("Press 1 to confirm the deletion: ");
                                int confirmdelete = Convert.ToInt32(Console.ReadLine());
                                if (confirmdelete == 1)
                                {
                                    bool delete = false;
                                    try
                                    {
                                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                                        {
                                            string line;
                                            List<string> words = new List<string>();
                                            while ((line = sr.ReadLine()) != null)
                                            {

                                                words = (line.Split('|')).ToList();
                                                bookcode = Convert.ToInt32(words[0]);
                                                if (id == bookcode)
                                                {
                                                    line_to_delete = line;
                                                    delete = true;
                                                    break;
                                                }
                                                else
                                                {
                                                    delete = false;
                                                }
                                            }
                                            sr.Close();
                                        }
                                        if (!delete)
                                        {
                                            Console.WriteLine("You entered incorrect code");
                                        }
                                    }
                                    catch
                                    {
                                        Console.WriteLine("Error opening file!(Book.cs DeleteBook(1))");
                                    }
                                    string str = null;
                                    try
                                    {
                                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                                        {
                                            using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                                            {
                                                while ((str = sr.ReadLine()) != null)
                                                {
                                                    if (String.Compare(str, line_to_delete) == 0)
                                                    {
                                                        Console.WriteLine("\nSuccessful deleting.");
                                                        continue;
                                                    }
                                                    sw.WriteLine(str);
                                                }
                                                sw.Close();
                                            }
                                            sr.Close();
                                        }
                                        File.Delete(path);
                                        File.Move(pathtemp, path);

                                    }
                                    catch
                                    {
                                        Console.WriteLine("Error opening file!(Book.cs DeleteBook(2))");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("The delete operation was canceled.");
                                }
                                break;
                            }
                            catch(Exception)
                            {
                                Console.WriteLine("Input error.");
                            }
                        }                              
                    }
                    else
                    {
                        Console.WriteLine("Record does not exist.");
                    }
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
                                            
        }
        /// <summary>
        /// Змінює інформацію про книгу.
        /// </summary>
        public void EditBook()
        {
            while(true)
            {
                try
                {
                    Console.Write("Enter the code to change: ");
                    int id = Convert.ToInt32(Console.ReadLine());
                    if (SearchedBookOperation(id))
                    {
                        string line = null;
                        try
                        {
                            List<string> words = new List<string>();
                            using (StreamReader sr = new StreamReader(path, Encoding.Default))
                            {
                                using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                                {
                                    while ((line = sr.ReadLine()) != null)
                                    {
                                        words = (line.Split('|')).ToList();
                                        Divide(words);
                                        if (id == bookcode)
                                        {
                                            while(true)
                                            {
                                                try
                                                {
                                                   
                                                    Console.WriteLine("What to edit ? \n 1-Title \n 2-Author \n 3-Genre \n 4-Year of publication \n 5-Number of pages \n 6-Language \n 7-Quantity \n 8-Price \n 0 - Cancel editing\n");
                                                    int edit_chose = Convert.ToInt32(Console.ReadLine());
                                                    switch (edit_chose)
                                                    {
                                                        case 1:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New title: ");
                                                                    title = Console.ReadLine();
                                                                    if (title == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 2:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New author: ");
                                                                    _author = Console.ReadLine();
                                                                    if (_author == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 3:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New genre: ");
                                                                    _genre = Console.ReadLine();
                                                                    if (_genre == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 4:
                                                            {
                                                                while (true)
                                                                {
                                                                    try
                                                                    {
                                                                        Console.Write("New year of publication: ");
                                                                        _publicationyear = Convert.ToInt32(Console.ReadLine());
                                                                        break;
                                                                    }
                                                                    catch (Exception)
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 5:
                                                            {
                                                                while (true)
                                                                {
                                                                    try
                                                                    {
                                                                        Console.Write("New number of pages: ");
                                                                        _numberpages = Convert.ToInt32(Console.ReadLine());
                                                                        break;
                                                                    }
                                                                    catch (Exception)
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 6:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New language: ");
                                                                    _language = Console.ReadLine();
                                                                    if (_language == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 7:
                                                            {
                                                                while (true)
                                                                {
                                                                    try
                                                                    {
                                                                        Console.Write("New quantity: ");
                                                                        quantity = Convert.ToInt32(Console.ReadLine());
                                                                        break;
                                                                    }
                                                                    catch (Exception)
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 8:
                                                            {
                                                                while (true)
                                                                {
                                                                    try
                                                                    {
                                                                        Console.Write("Enter the price: ");
                                                                        price = Convert.ToDouble(Console.ReadLine());
                                                                        break;
                                                                    }
                                                                    catch (Exception)
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 0:
                                                            {
                                                                Console.Write("Operation canceled. ");
                                                            }
                                                            break;
                                                        default:
                                                            {
                                                                Console.WriteLine("You entered incorrect number.");
                                                            }
                                                            break;
                                                    }
                                                    Console.WriteLine("\n");
                                                    PrintDataBook();
                                                    DataToString();
                                                    sw.WriteLine(datastring);
                                                    break;
                                                }
                                                catch(Exception)
                                                {
                                                    Console.WriteLine("Input error.");
                                                }
                                            }
                                            
                                        }
                                        else
                                            if (id != _bookcode)
                                            {
                                            sw.WriteLine(line);

                                            }
                                    }
                                    sw.Close();
                                }
                                sr.Close();
                            }
                            File.Delete(path);
                            File.Move(pathtemp, path);
                        }
                        catch
                        {
                            Console.WriteLine("Error opening file! (Book.cs EditBook())");
                        }

                    }
                    else
                    {
                        Console.WriteLine("Record does not exist.");
                    }
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
                       
        }
        /// <summary>
        /// Змінює кількість книг.
        /// </summary>
        public void EditNumberCopies(int number, int id)
        {
            string line = null;
            try
            {
                List<string> words = new List<string>();
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                    {
                        while ((line = sr.ReadLine()) != null)
                        {

                            words = (line.Split('|')).ToList();
                            Divide(words);
                            if (id == bookcode)
                            {
                                quantity -= number;
                                DataToString();
                                sw.WriteLine(datastring);
                            }
                            else
                                if (id != bookcode)
                            {
                                sw.WriteLine(line);

                            }
                        }
                        sw.Close();
                    }
                    sr.Close();
                }
                File.Delete(path);
                File.Move(pathtemp, path);
            }
            catch
            {
                Console.WriteLine("Error opening file!(Book.cs EditNumberCopies())");
            }
        }
        /// <summary>
        /// Ділить запис в нашому файлі на слова.
        /// </summary>
        /// <param name="words"></param>
        private void Divide(List<string> words)
        {
            bookcode = Convert.ToInt32(words[0]);
            title = words[1];
            _author = words[2];
            _genre = words[3];
            _publicationyear = Convert.ToInt32(words[4]);
            _numberpages = Convert.ToInt32(words[5]);
            _language = words[6];
            quantity = Convert.ToInt32(words[7]);
            price = Convert.ToDouble(words[8]);
        }
    }  
}
