﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace LabNumber4_Book_
{
    class Employee : Person
    {
        private string _position;
        private string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Employee.txt");
        private string pathtemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp.txt");
        private string datastring;
       
        /// <summary>
        /// Записуємо дані про нашого працівника у вигляді строки в такому форматі.
        /// </summary>
        public void DataToString()
        {
            string datastring = $"{perscode}|{surname}|{name}|{_phonenumber}|{_email}|{_position}";
            this.datastring = datastring;
        }

        /// <summary>
        /// Запрошуємо ввід даних із клавіатури.
        /// </summary>
        public void EnterData()
        {
            while (true)
            {
                Console.Write("Enter surname: ");
                surname = Console.ReadLine();
                if (surname == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                Console.Write("Enter name: ");
                name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                Console.Write("Enter phone number: ");
                _phonenumber = Console.ReadLine();
                if (_phonenumber == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                Console.Write("Enter email: ");
                _email = Console.ReadLine();
                if (_email == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                Console.Write("Enter position: ");
                _position = Console.ReadLine();
                if (_position == "")
                {
                    Console.WriteLine("Input error.");
                }
                else
                {
                    break;
                }
            }
            
        }
        /// <summary>
        /// Створюємо нового працівника і записуємо його дані в файл.
        /// </summary>

        public override void AddNewPerson()
        {
            while(true)
            {
                try
                {
                    int id = GetPerscode();
                    perscode = id;
                    EnterData();

                    DataToString();
                    using (StreamWriter streamWriter = new StreamWriter(path, true))
                    {
                        streamWriter.WriteLine(datastring);

                        streamWriter.Close();
                    }
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
            
        }
        
        /// <summary>
        /// Виводить інформацію про всіх працівників.
        /// </summary>
        public override void PrintAll()
        {
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        PrintDataEmployee();
                    }
                    sr.Close();
                }
            }
            catch
            {
                Console.WriteLine("Error opening file!(Employee.cs PrintAll())");
            }
        }
        /// <summary>
        /// Виводить інформацію про одного працівника.
        /// </summary>
        private void PrintDataEmployee()
        {
            Console.WriteLine("Employee ID: " + perscode);
            Console.WriteLine("Surname: " + surname);
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Phone number: " + _phonenumber);
            Console.WriteLine("Email: " + _email);
            Console.WriteLine("Position: " + _position);
            Console.WriteLine("\n");
        }
        /// <summary>
        /// Шукає працівника серед файлу.
        /// </summary>
        /// <param name="searchedcode"></param>
        /// /// <returns>True or False</returns>
        public override bool SearchPersonOperation(int searchedcode)
        {
            bool search = false;
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        if (searchedcode == perscode)
                        {
                            Console.WriteLine("Success! Record found. ");
                            search = true;
                            PrintDataEmployee();
                            break;
                        }
                        else
                        {
                            search = false;
                        }
                    }
                    sr.Close();

                }
            }
            catch
            {
                Console.WriteLine("Error opening file! (Employee.cs SearchPersonOperation()) ");
            }
            if (!search)
            {
                Console.WriteLine("Failed");
                return false;
            }
            return true;
        }
        public void Search()
        {
            
            while (true)
            {
                try
                {
                    Console.Write(" Enter code: ");
                    int searchedcode = Convert.ToInt32(Console.ReadLine());
                    SearchPersonOperation(searchedcode);
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Input error.");
                }
            }
        }
        /// <summary>
        /// Видаляє запис про працівника.
        /// </summary>
        public void Delete()
        {
            DeletePerson(path, pathtemp);
        }
        /// <summary>
        /// Змінює інформацію про працівника.
        /// </summary>
        public override void EditPerson()
        {
            while(true)
            {
                try
                {
                    Console.Write("Enter the code to change: ");
                    int id = Convert.ToInt32(Console.ReadLine());
                    if (SearchPersonOperation(id))
                    {
                        string line = null;
                        try
                        {
                            List<string> words = new List<string>();
                            using (StreamReader sr = new StreamReader(path, Encoding.Default))
                            {
                                using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                                {
                                    while ((line = sr.ReadLine()) != null)
                                    {
                                        words = (line.Split('|')).ToList();
                                        Divide(words);
                                        if (id == perscode)
                                        {
                                            while(true)
                                            {
                                                try
                                                {
                                                    Console.WriteLine("What to edit ? \n 1-Surname \n 2-Name \n 3-Phone number \n 4-Email \n 5-Position \n 0-Cancel operation.\n");
                                                    int edit_chose = Convert.ToInt32(Console.ReadLine());
                                                    switch (edit_chose)
                                                    {
                                                        case 1:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New surname: ");
                                                                    surname = Console.ReadLine();
                                                                    if (surname == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 2:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New name: ");
                                                                    name = Console.ReadLine();
                                                                    if (name == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 3:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New phone number: ");
                                                                    _phonenumber = Console.ReadLine();
                                                                    if (_phonenumber == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }

                                                            }
                                                            break;
                                                        case 4:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New email: ");
                                                                    _email = Console.ReadLine();
                                                                    if (_email == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 5:
                                                            {
                                                                while (true)
                                                                {
                                                                    Console.Write("New position: ");
                                                                    _position = Console.ReadLine();
                                                                    if (_position == "")
                                                                    {
                                                                        Console.WriteLine("Input error.");
                                                                    }
                                                                    else
                                                                    {
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        case 0:
                                                            {
                                                                Console.Write("Operation canceled. ");
                                                            }
                                                            break;
                                                        default:
                                                            {
                                                                Console.WriteLine("You entered incorrect number.");
                                                            }
                                                            break;
                                                    }
                                                    Console.WriteLine("\n");
                                                    PrintDataEmployee();
                                                    DataToString();
                                                    sw.WriteLine(datastring);
                                                    break;
                                                }
                                                catch(Exception)
                                                {
                                                    Console.WriteLine("Input error.");
                                                }
                                            }
                                            
                                        }
                                        else
                                            if (id != perscode)
                                        {
                                            sw.WriteLine(line);
                                        }
                                    }
                                    sw.Close();
                                }
                                sr.Close();
                            }
                            File.Delete(path);
                            File.Move(pathtemp, path);
                        }
                        catch
                        {
                            Console.WriteLine("Error opening file! (Employee.cs EditPerson())");
                        }

                    }
                    else
                    {
                        Console.WriteLine("Record does not exist.");
                    }
                    break;
                }

                catch(Exception)
                {
                    Console.WriteLine("Input error.");
                }
               
            }
            
        }
        /// <summary>
        /// Ділить запис в нашому файлі на слова.
        /// </summary>
        /// <param name="words"></param>
        private void Divide(List<string> words)
        {
            perscode = Convert.ToInt32(words[0]);
            surname = words[1];
            name = words[2];
            _phonenumber = words[3];
            _email = words[4];
            _position = words[5];
        }
    }
}
