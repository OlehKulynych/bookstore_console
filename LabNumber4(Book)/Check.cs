﻿using System;
namespace LabNumber4_Book_
{
    enum OrderStatus : int { New, Paid, Canceled}
    class Check : Purchase
    {
        /// <summary>
        /// Визначаємо статус замовлення та друкуємо чек.
        /// </summary>
        public void CheckOperation()
        {
            OrderStatus orderStatus;
            Order();
            if(!isOrderEmpty)
            {
                Console.Clear();
                orderStatus = OrderStatus.New;
                Console.WriteLine("-----------------");
                Console.WriteLine("Order Status: " + orderStatus);
                Console.WriteLine("-----------------");
                PrintCheck(check_books);
                Console.WriteLine("-----------------");
                bool status = true;
                do
                {
                    try
                    {
                        Console.WriteLine("\nChange order status :\n 1 - Paid\n 2 - Canceled\n");
                        Console.Write("Enter: ");
                        int statusselectoin = Convert.ToInt32(Console.ReadLine());
                        switch (statusselectoin)
                        {
                            case 1:
                                {
                                    Console.Clear();
                                    orderStatus = OrderStatus.Paid;
                                    Console.WriteLine("-----------------");
                                    Console.WriteLine("Order Status: " + orderStatus);
                                    Console.WriteLine("-----------------");
                                    PrintCheck(check_books);
                                    Console.WriteLine("-----------------");
                                    status = false;
                                }
                                break;
                            case 2:
                                {
                                    Console.Clear();
                                    orderStatus = OrderStatus.Canceled;
                                    Console.WriteLine("-----------------");
                                    Console.WriteLine("Order Status: " + orderStatus);
                                    Console.WriteLine("-----------------");
                                    PrintCheck(check_books);
                                    Console.WriteLine("-----------------");
                                    Book book = new Book();
                                    int i = 0;
                                    foreach (Purchase p in check_books)
                                    {
                                        book.EditNumberCopies(-(_numberbooks[i]), p.checkbookid);
                                        i++;
                                    }
                                    status = false;
                                }
                                break;
                            default:
                                {
                                    Console.WriteLine("You enter an incorrect number, repeat ...");
                                }
                                break;
                        }
                    }
                    catch(Exception)
                    {
                        Console.WriteLine("Input error.");
                    }
                   
                } while (status == true);
            }
            Console.ReadKey();
        }
    }
}
